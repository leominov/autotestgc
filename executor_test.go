package main

import "testing"

type fakeExecutor struct {
	output []byte
	err    error
}

func newFakeExecutor(output []byte, err error) executor {
	return &fakeExecutor{output, err}
}

func (a *fakeExecutor) Exec(_ []string) ([]byte, error) {
	return a.output, a.err
}

func TestExec(t *testing.T) {
	e := &realExecutor{
		bin: "foobar",
	}
	_, err := e.Exec(nil)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	e = &realExecutor{
		bin: "whoami",
	}
	_, err = e.Exec(nil)
	if err != nil {
		t.Error(err)
	}
}
