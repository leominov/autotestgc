package main

import (
	"fmt"
	"os/exec"
)

type executor interface {
	Exec(args []string) ([]byte, error)
}

type realExecutor struct {
	bin string
}

func newRealExecutor() (executor, error) {
	bin, err := exec.LookPath("argocd")
	if err != nil {
		return nil, err
	}
	return &realExecutor{bin}, nil
}

func (a *realExecutor) Exec(args []string) ([]byte, error) {
	output, err := exec.Command(a.bin, args...).CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("%v: %s", err, string(output))
	}
	return output, nil
}
