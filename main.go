package main

import (
	"flag"

	"github.com/golang/glog"
	"k8s.io/klog"
)

var (
	dryRun   = flag.Bool("dry-run", false, "Do not delete anything.")
	ttlMap   = flag.String("ttl-map", DefaultTTLMap, "Time to live for temporary resources. (e.g. NamespaceNameGlob=Time.Duration)")
	selector = flag.String("selector", DefaultArgoCDStatesflowNamespaceFilter, "Selector (label query) to filter on, supports '=', '==', and '!='. (e.g. -l key1=value1,key2=value2)")
)

func main() {
	flag.Parse()

	klog.Info("Starting gc...")

	gc, err := NewGC(*ttlMap, *selector, *dryRun)
	if err != nil {
		glog.Fatal(err)
	}

	err = gc.Run()
	if err != nil {
		glog.Fatal(err)
	}

	klog.Infoln("Done")
}
