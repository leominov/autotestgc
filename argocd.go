package main

import (
	"encoding/json"
	"fmt"
)

type ArgoCDClient struct {
	executor executor
}

type UserInfo struct {
	LoggedIn bool     `json:"loggedIn"`
	Username string   `json:"username"`
	ISS      string   `json:"iss"`
	Groups   []string `json:"groups"`
}

func NewArgoCDClient() (*ArgoCDClient, error) {
	executor, err := newRealExecutor()
	if err != nil {
		return nil, err
	}
	return &ArgoCDClient{
		executor: executor,
	}, nil
}

func (a *ArgoCDClient) AccountGetUserInfo() (*UserInfo, error) {
	args := []string{
		"account",
		"get-user-info",
		"-o",
		"json",
	}
	b, err := a.executor.Exec(args)
	if err != nil {
		return nil, fmt.Errorf("failed to get user info: %v", err)
	}
	ui := &UserInfo{}
	return ui, json.Unmarshal(b, ui)
}

func (a *ArgoCDClient) AppDelete(name string) error {
	// --cascade – Perform a cascaded deletion of all application resources (default true)
	args := []string{
		"app",
		"delete",
		name,
		"--cascade=false",
	}
	if _, err := a.executor.Exec(args); err != nil {
		return fmt.Errorf("failed to delete: %v", err)
	}
	return nil
}
