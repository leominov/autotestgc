package main

import (
	"errors"
	"os/exec"
	"testing"
)

func TestNewArgoCDClient(t *testing.T) {
	_, errLookPath := exec.LookPath("argocd")
	_, err := NewArgoCDClient()
	if errLookPath != nil && err == nil {
		t.Error("Must be en error, but got nil")
	}
	if errLookPath == nil && err != nil {
		t.Error(err)
	}
}

func TestAccountGetUserInfo(t *testing.T) {
	a := ArgoCDClient{
		executor: newFakeExecutor(nil, errors.New("failed")),
	}
	_, err := a.AccountGetUserInfo()
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	a = ArgoCDClient{
		executor: newFakeExecutor([]byte(`{"loggedIn": true}`), nil),
	}
	ui, err := a.AccountGetUserInfo()
	if err != nil {
		t.Error(err)
	}
	if !ui.LoggedIn {
		t.Error("Must be true, but got false")
	}
}

func TestAppDelete(t *testing.T) {
	a := ArgoCDClient{
		executor: newFakeExecutor(nil, errors.New("failed")),
	}
	err := a.AppDelete("foobar")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	a = ArgoCDClient{
		executor: newFakeExecutor(nil, nil),
	}
	err = a.AppDelete("foobar")
	if err != nil {
		t.Error(err)
	}
}
