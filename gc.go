package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/gobwas/glob"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/klog"
)

const (
	// DefaultTTLMap значение по умолчанию для всех Namespace подходящих под фильтр из константы
	// DefaultArgoCDStatesflowNamespaceFilter
	DefaultTTLMap = "30m"

	// Имя и значение лейбла, по которым будет производиться фильтрация имеющихся Namespace
	DefaultArgoCDStatesflowNamespaceFilter = "advert.tinkoff.ru/argocd-statesflow-environment-type=dynamic"

	// Имя лейбла, где хранится название приложения ArgoCD, например:
	//
	// * advert.tinkoff.ru/argocd-statesflow-application=pfa-action-log-production
	//
	// Если TTL был достигнут, но лейбл содержал некорректное значение или несуществующие приложение
	// в ArgoCD, то Namespace удален не будет (смотри cleanupNamespaces), если лейбл отсутствовал, то
	// процедура удаления приложения будет пропущена
	ArgoCDStatesflowApplicationLabels = "advert.tinkoff.ru/argocd-statesflow-application"
)

type GC struct {
	kubeCli  *kubernetes.Clientset
	argoCli  *ArgoCDClient
	dryRun   bool
	ttls     []*TTL
	selector string
}

type TTL struct {
	Text     string
	Selector glob.Glob
	Duration time.Duration
}

func NewGC(ttlMap string, selector string, dryRun bool) (*GC, error) {
	ttls, err := ParseTTLMap(ttlMap)
	if err != nil {
		return nil, err
	}
	for _, ttl := range ttls {
		klog.Infof("TTL item: %s", ttl.String())
	}
	argoCli, err := NewArgoCDClient()
	if err != nil {
		return nil, err
	}
	ui, err := argoCli.AccountGetUserInfo()
	if err != nil {
		return nil, err
	}
	klog.Infof("ArgoCD user info: %#v", ui)
	kubeCli, err := getKubeClient()
	if err != nil {
		return nil, err
	}
	return &GC{
		argoCli:  argoCli,
		kubeCli:  kubeCli,
		dryRun:   dryRun,
		ttls:     ttls,
		selector: selector,
	}, nil
}

func parseTTLItem(item string) (*TTL, error) {
	pairs := strings.Split(strings.TrimSpace(item), "=")
	if len(pairs) != 2 {
		return nil, fmt.Errorf("incorrect format of %q entry", item)
	}
	g, err := glob.Compile(pairs[0])
	if err != nil {
		return nil, err
	}
	d, err := time.ParseDuration(pairs[1])
	if err != nil {
		return nil, err
	}
	return &TTL{
		Text:     pairs[0],
		Selector: g,
		Duration: d,
	}, nil
}

func ParseTTLMap(ttlMap string) ([]*TTL, error) {
	var ttls []*TTL
	if d, err := time.ParseDuration(ttlMap); err == nil {
		return []*TTL{
			{
				Text:     "*",
				Selector: glob.MustCompile("*"),
				Duration: d,
			},
		}, nil
	}
	ttlDataArr := strings.Split(ttlMap, ",")
	for _, ttlData := range ttlDataArr {
		ttl, err := parseTTLItem(ttlData)
		if err != nil {
			return nil, err
		}
		ttls = append(ttls, ttl)
	}
	return ttls, nil
}

func (g *GC) Run() error {
	if g.dryRun {
		klog.Infoln("Runs in dry-run mode")
	}
	klog.Infoln("Cleaning up namespaces...")
	if err := g.cleanupNamespaces(); err != nil {
		return fmt.Errorf("cleanup failed: %v", err)
	}
	return nil
}

func (g *GC) cleanupNamespaces() error {
	namespaces, err := g.getNamespaceWithTTLExceeded()
	if err != nil {
		return err
	}
	for _, namespace := range namespaces.Items {
		klog.Infof(
			"Found '%s' namespace with TTL exceeded (Timestamp: %s, Labels: %v)",
			namespace.Name, namespace.CreationTimestamp, namespace.Labels,
		)
		if err := g.deleteApplication(namespace); err != nil {
			klog.Error(err)
		}
		if err := g.deleteNamespace(namespace); err != nil {
			klog.Error(err)
		}
	}
	return nil
}

func (g *GC) getDeleteOptions() *metav1.DeleteOptions {
	opts := &metav1.DeleteOptions{}
	if g.dryRun {
		opts.DryRun = []string{
			metav1.DryRunAll,
		}
	}
	return opts
}

func (g *GC) deleteApplication(namespace v1.Namespace) error {
	app, ok := namespace.Labels[ArgoCDStatesflowApplicationLabels]
	if g.dryRun || !ok {
		return nil
	}
	return g.argoCli.AppDelete(app)
}

func (g *GC) deleteNamespace(namespace v1.Namespace) error {
	opts := g.getDeleteOptions()
	return g.kubeCli.CoreV1().Namespaces().Delete(namespace.Name, opts)
}

func (g *GC) getTTLByName(name string) *TTL {
	for _, ttl := range g.ttls {
		if ttl.Selector.Match(name) {
			return ttl
		}
	}
	return nil
}

func (g *GC) IsNamespaceTTLExceeded(namespace v1.Namespace) bool {
	ttl := g.getTTLByName(namespace.Name)
	if ttl == nil {
		return false
	}
	creatingTime := namespace.CreationTimestamp.Time
	return time.Now().After(creatingTime.Add(ttl.Duration))
}

func (g *GC) getNamespaceWithTTLExceeded() (*v1.NamespaceList, error) {
	opts := metav1.ListOptions{
		LabelSelector: g.selector,
	}
	resp, err := g.kubeCli.CoreV1().Namespaces().List(opts)
	if err != nil {
		return nil, err
	}
	result := &v1.NamespaceList{}
	for _, namespace := range resp.Items {
		if !g.IsNamespaceTTLExceeded(namespace) {
			continue
		}
		result.Items = append(result.Items, namespace)
	}
	return result, nil
}

func getKubeClient() (*kubernetes.Clientset, error) {
	var config *rest.Config
	config, err := rest.InClusterConfig()
	if err != nil {
		config, err = clientcmd.BuildConfigFromFlags(
			"", os.Getenv("HOME")+"/.kube/config",
		)
		if err != nil {
			return nil, err
		}
	}
	return kubernetes.NewForConfig(config)
}

func (t *TTL) String() string {
	if len(t.Text) == 0 {
		return t.Duration.String()
	}
	return fmt.Sprintf("%q: %s", t.Text, t.Duration.String())
}
