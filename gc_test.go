package main

import (
	"reflect"
	"testing"
	"time"

	"github.com/gobwas/glob"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestIsNamespaceTTLExceeded(t *testing.T) {
	gc := &GC{
		ttls: []*TTL{
			{
				Selector: glob.MustCompile("foobar*"),
				Duration: 10 * time.Minute,
			},
		},
	}
	tests := []struct {
		ns       v1.Namespace
		exceeded bool
	}{
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "foobar",
					CreationTimestamp: metav1.NewTime(time.Now()),
				},
			},
			exceeded: false,
		},
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "foobar",
					CreationTimestamp: metav1.NewTime(time.Now().Add(-5 * time.Minute)),
				},
			},
			exceeded: false,
		},
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "foobar",
					CreationTimestamp: metav1.NewTime(time.Now().Add(-10 * time.Minute)),
				},
			},
			exceeded: true,
		},
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "foobar-abcd",
					CreationTimestamp: metav1.NewTime(time.Now().Add(-10 * time.Minute)),
				},
			},
			exceeded: true,
		},
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "abcd",
					CreationTimestamp: metav1.NewTime(time.Now().Add(-10 * time.Minute)),
				},
			},
			exceeded: false,
		},
		{
			ns: v1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:              "foobar",
					CreationTimestamp: metav1.NewTime(time.Now().Add(10 * time.Minute)),
				},
			},
			exceeded: false,
		},
	}
	for id, test := range tests {
		exceeded := gc.IsNamespaceTTLExceeded(test.ns)
		if test.exceeded != exceeded {
			t.Errorf("%d. Must be %v, but got %v for %v", id, test.exceeded, exceeded, test.ns)
		}
	}
}

func TestParseTTLMap(t *testing.T) {
	tests := []struct {
		in  string
		out []*TTL
	}{
		{
			in:  "",
			out: nil,
		},
		{
			in:  "*=foobar",
			out: nil,
		},
		{
			in: "30m",
			out: []*TTL{
				{
					Text:     "*",
					Selector: glob.MustCompile("*"),
					Duration: 30 * time.Minute,
				},
			},
		},
		{
			in: "*=30m",
			out: []*TTL{
				{
					Text:     "*",
					Selector: glob.MustCompile("*"),
					Duration: 30 * time.Minute,
				},
			},
		},
		{
			in: "foobar=15m,*=30m",
			out: []*TTL{
				{
					Text:     "foobar",
					Selector: glob.MustCompile("foobar"),
					Duration: 15 * time.Minute,
				},
				{
					Text:     "*",
					Selector: glob.MustCompile("*"),
					Duration: 30 * time.Minute,
				},
			},
		},
	}
	for id, test := range tests {
		out, _ := ParseTTLMap(test.in)
		if !reflect.DeepEqual(out, test.out) {
			t.Errorf("%d. %v not equal to %v", id, out, test.out)
		}
	}
}

func TestTTL_String(t *testing.T) {
	tests := map[string]TTL{
		`"*": 30m0s`: {
			Text:     "*",
			Selector: glob.MustCompile("*"),
			Duration: 30 * time.Minute,
		},
		`30m0s`: {
			Selector: glob.MustCompile("*"),
			Duration: 30 * time.Minute,
		},
	}
	for out, in := range tests {
		if in.String() != out {
			t.Errorf("Must be %s, but got %s", out, in.String())
		}
	}
}
