FROM golang:1.13-alpine as builder
WORKDIR /go/src/gitlab-rnd.tcsbank.ru/devops/autotestgc
COPY . .
RUN CGO_ENABLED=0 go build

FROM alpine:3.10
ENV ARGOCD_VERSION="v1.7.8"
RUN wget -O /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/download/${ARGOCD_VERSION}/argocd-linux-amd64 && \
    chmod +x /usr/local/bin/argocd
COPY --from=builder /go/src/gitlab-rnd.tcsbank.ru/devops/autotestgc/autotestgc /go/bin/autotestgc
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
CMD ["/go/bin/autotestgc"]
