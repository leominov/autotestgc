# Autotest Garbage Collector

CronJob для автоматического удаления Namespace, созданных для проведения автотестов, в Kubernetes-кластере [pfa-dev/qa-gke-cluster](https://console.cloud.google.com/kubernetes/clusters/details/europe-north1-a/qa-gke-cluster?project=pfa-dev-211513).

Основные моменты:

* проверка осуществляется каждые 15 минут;
* отслеживаются только Namespace с лейблом `advert.tinkoff.ru/argocd-statesflow-environment-type=dynamic`;
* по значению лейба `advert.tinkoff.ru/argocd-statesflow-application` происходит удаление из ArgoCD.

## Аргументы запуска

* `dry-run` – пробный запуск, ничто не будет удалено (`false`);
* `selector` – label-селектор для поиска Namespace (`advert.tinkoff.ru/argocd-statesflow-environment-type=dynamic`);
* `ttl-map` – предельное время жизни Namespace, задается в виде `NamespaceNameGlob=time.Duration` или `time.Duration` (`30m`).

В качестве пояснения назначения `ttl-map` возьмем значение `autotest-*=60m,review-*=48h` – это реальное значение из конфигурации для удаления временных окружений в тестовой среде PFA. В данном случае приложение будет следить за всеми Namespace из заданного селектора и если время жизни Namespace с именем по маске `autotest-*` (например, `autotest-pfa-auth`) будет больше часа, то оно будет удалено, по аналогии указано второе правило – если имя Namespace подходит под маску `review-*` (например, `review-pfa-blocks-AABBCCDD`), а время жизни меньше 2 суток, то Namespace продолжит жить, если нет – будет удален,
