IMG = eu.gcr.io/utilities-212509/autotestgc
TAG := $(shell date +v%Y%m%d)-$(shell git describe --tags --always --dirty)

.PHONY: build
build:
	docker build ./ --tag "$(IMG):$(TAG)"

.PHONY: push
push:
	docker push "$(IMG):$(TAG)"

.PHONY: render
render:
	@kustomize build kustomize/overlays/pfa-dev

.PHONY: deploy
deploy:
	@kustomize build kustomize/overlays/pfa-dev | kubectl apply -f -

.PHONY: test
test:
	@go test ./ --cover

.PHONY: test-report
test-report:
	@go test ./ -coverprofile=coverage.out && go tool cover -html=coverage.out
